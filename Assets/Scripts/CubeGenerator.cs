using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Objet contenant un nom de musique et une liste de cube
//Utilisé pour la génération et la lecture de fichiers XML de chorégraphies
public class CubeGenerator
{
    public string musicName;
    public List<MusicCube> musicCubes = new List<MusicCube>();
}
