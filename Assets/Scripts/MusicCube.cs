using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Contient les informations n�cessaires pour g�n�rer les cubes
//Utilis� pour les XML de chor�graphie
public class MusicCube
{
    public int type;
    public int color;
    public int posSpawn;
    public float timeSpawn;
    public int rotation;
    public int index;
}
