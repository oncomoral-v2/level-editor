using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Permet de modifier l'image dans le menu d'ajout pour correspondre au cube que l'on souhaite ajouter
//Appliqu� sur l'objet Cube dans le sous-menu CubeAdder
//Appel� par RotateButton et ChangeColorButton
public class MenuCubeImage : MonoBehaviour
{
    public Image cubeImage;

    public Image colorRectangle;

    public Image buttonImage;

    private bool isBlue = true;

    private int rot = 0;

    public void ChangeColor()
    {
        //Change la couleur du cube � ajouter
        isBlue = !isBlue;
        if (isBlue)
        {
            colorRectangle.color = Color.blue;
            buttonImage.color = Color.red;
        }
        else
        {
            colorRectangle.color = Color.red;
            buttonImage.color = Color.blue;
        }
    }

    public void Rotate()
    {
        //Change la rotation du cube � ajouter
        if (rot < 3)
        {
            rot++;
        }
        else
        {
            rot = 0;
        }

        cubeImage.rectTransform.rotation = Quaternion.Euler(new Vector3(cubeImage.rectTransform.rotation.x, cubeImage.rectTransform.rotation.y, 90 * rot));
    }
}
