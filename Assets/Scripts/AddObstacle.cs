using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//G�re l'ajout des cubes dans l'�diteur de niveaux
//Appliqu� sur l'objet CubePlacer dans la sc�ne
//Appel� lorsqu'on appuie sur les diff�rents boutons du sous-menu CubeAdder
public class AddObstacle : MonoBehaviour
{
    public GameObject obstacleObject;
    public Transform[] points;
    private float time;
    public GameObject cubeParent;
    public Slider mainSlider;
    public MusicSlider musicSlider;
    public GameManager gameManager;

    public Image obstacleImage;

    private int rot = 0;

    public void PlaceObstacle(int pos)
    {
        //Ajoute un obstacle � la position pos, et la rotation rot
        //La rotation est impos�e par le placement de l'obstacle
        rot = 0;
        if (pos == 3)
        {
            rot = 1;
        }
        float localPosz = 12;
        float halfLength = obstacleObject.transform.localScale.z / 2;
        float offSet = (localPosz - halfLength) / GameManager.cubeSpeed;
        GameObject obstacle = Instantiate(obstacleObject, points[pos]);
        obstacle.transform.localPosition = new Vector3(0, 0, -localPosz + halfLength);
        obstacle.transform.parent = cubeParent.transform;
        obstacle.GetComponent<Movement>().mainSlider = mainSlider;
        obstacle.GetComponent<Movement>().musicSlider = musicSlider;
        obstacle.transform.Rotate(transform.forward, 90 * rot);
        MusicCube musicCube = new MusicCube();
        musicCube.type = 1;
        musicCube.posSpawn = pos;
        musicCube.rotation = rot;
        musicCube.timeSpawn = GameManager.timer - offSet;
        obstacle.GetComponent<Movement>().startPos = points[pos].position;
        obstacle.GetComponent<Movement>().startPos.z += GameManager.cubeSpeed * musicCube.timeSpawn;
        //cube.GetComponent<Movement>().timer = GameManager.timer;
        GameManager.currentCubes.Add(obstacle);
        //musicCube.index = GameManager.currentCubes.Count;
        GameManager.musicCubes.Add(musicCube);
        gameManager.Sort();
    }

    /*public void Rotate()
    {
        //Change la rotation du cube � ajouter
        obstacleImage.rectTransform.rotation = Quaternion.Euler(new Vector3(obstacleImage.rectTransform.rotation.x, obstacleImage.rectTransform.rotation.y, 90));
    }*/
}
