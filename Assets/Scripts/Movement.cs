using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//Permet d'avancer les cubes et stocke leur position initiale
//Directement appliqu� sur les cubes
public class Movement : MonoBehaviour
{
    public Slider mainSlider;

    public MusicSlider musicSlider;

    //public float timer;

    public int id;

    public Vector3 startPos;

    private float length;

    // Start is called before the first frame update
    void Start()
    {
        //Pour les cubes ajout�s, startPos prend une valeur non nulle
        //Bricolage tr�s moche
        if (startPos == new Vector3(0, 0, 0))
        {
            startPos = transform.position;
        }
        
        length = musicSlider.length;
    }

    // Update is called once per frame
    void Update()
    {
        //Avance les cubes lorsqu'on n'est pas en mode placement
        if (!GameManager.inCubePlacement)
        {
            transform.position = -GameManager.timer * transform.forward * GameManager.cubeSpeed + startPos;
        }

    }

}