using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//Permet de g�rer l'avancement de la musique et de la chor�graphie
//Appliqu� sur l'objet Slider dans Canvas
//Appel� lorsqu'on clique sur le slider
public class MusicSlider : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    public AudioSource audioSource;

    public AudioClip audioClip;

    public float length;

    public Slider mainSlider;

    public Spawner spawner;

    float initValue;

    private bool click = false;

    void Start()
    {
        length = audioClip.length;
    }

    void Update()
    {
        //Bug : quand le slider arrive au bout, sa valeur reste bloqu�e � 0
        if (!click && !GameManager.inCubePlacement)
        {
            mainSlider.value = audioSource.time / length;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (GameManager.inCubePlacement)
        {
            mainSlider.value = initValue;
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        initValue = mainSlider.value;
        click = true;
        //Debug.Log(initValue);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        /*foreach (GameObject cube in GameManager.currentCubes)
        {
            //cube.GetComponent<Movement>().timer += (mainSlider.value - initValue) * length;
            cube.GetComponent<Movement>().timer = mainSlider.value * length;
        }*/
        //GameManager.timer += (mainSlider.value - initValue) * length;
        GameManager.timer = mainSlider.value * length;

        //spawner.timer = mainSlider.value * length;
        audioSource.time = mainSlider.value * length;
        click = false;
    }

    //public void Reset()
    //{
    //    /*foreach (GameObject cube in GameManager.currentCubes)
    //    {
    //        cube.GetComponent<Movement>().timer = 0;
    //    }*/

    //    GameManager.timer = 0;
    //    audioSource.time = 0;
    //    mainSlider.value = 0;
    //}
}
