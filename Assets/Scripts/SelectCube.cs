using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Permet de s�lectionner un cube et de l'ajouter dans une liste selectedCubes du GameManager
//Appliqu� sur les cubes
//Appel� lorsqu'on clique sur un cube
public class SelectCube : MonoBehaviour
{
    private Material material;

    private bool isSelected;

    void Start()
    {
        material = gameObject.GetComponent<Renderer>().material;
        //timer = gameObject.GetComponent<Movement>().timer;
    }

    public void OnMouseDown()
    {
        if (GameManager.inCubePlacement && !GameManager.inCubeAdding)
        {
            isSelected = !isSelected;
            if (isSelected)
            {
                GameManager.selectedCubes.Add(gameObject);
                material.color = Color.green;
                //GameManager.nCubesSelected++;
            }
            else
            {
                GameManager.selectedCubes.Remove(gameObject);
                material.color = Color.black;
                //GameManager.nCubesSelected--;
            }
        }
    }

    /*void Update()
    {
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            DeleteCube();
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            MoveBackward();
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MoveForward();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            isSelected = false;
            material.color = Color.black;
            GameManager.nCubesSelected = 0;
        }
    }*/

    /*private void DeleteCube()
    {
        if (isSelected)
        {
            GameManager.currentCubes.Remove(gameObject);
            Destroy(gameObject);
        }
            
    }

    private void MoveForward()
    {
        //Le cube arrive plus un dixi�me de seconde plus t�t
        if (isSelected)
        {
            gameObject.transform.position -= transform.forward * 0.1f * cubeSpeed;
            gameObject.GetComponent<Movement>().timer += 0.1f;
            Debug.Log(gameObject.GetComponent<Movement>().timer);
        }
    }

    private void MoveBackward()
    {
        //Le cube arrive plus un dixi�me de seconde plus tard
        if (isSelected)
        {
            gameObject.transform.position += transform.forward * 0.1f * cubeSpeed;
            gameObject.GetComponent<Movement>().timer += 0.1f;
        }
    }*/
}
