using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Lit le fichier XML permettant de g�n�rer les cubes et g�n�re les cubes
//Appliqu� sur l'objet Spawner
//Appel� au lancement du jeu
public class Spawner : MonoBehaviour
{
    public GameObject[] cubes;
    public GameObject obstacleObject;
    public Transform[] points;
    public Transform[] obstaclePoints;
    public GameObject cubeParent;
    public Slider mainSlider;
    public MusicSlider musicSlider;
    private string musicName = "Test";
    private CubeGenerator cubeGenerator;
    private List<MusicCube> musicCubes;

    // Start is called before the first frame update
    void Start()
    {
        StartSpawn();
        PlaceCubes();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlaceCubes()
    {
        //Place les cubes � partir des informations lues dans le fichier XML
        int index = 0;
        foreach(MusicCube musicCube in musicCubes)
        {
            if (musicCube.type == 0)
            {
                GameObject cube = Instantiate(cubes[musicCube.color], points[musicCube.posSpawn]);
                cube.transform.localPosition = new Vector3(0, 0, GameManager.cubeSpeed * musicCube.timeSpawn);
                cube.transform.Rotate(transform.forward, 90 * musicCube.rotation);
                cube.transform.parent = cubeParent.transform;
                cube.GetComponent<Movement>().mainSlider = mainSlider;
                cube.GetComponent<Movement>().musicSlider = musicSlider;
                cube.GetComponent<Movement>().id = index;
                GameManager.currentCubes.Add(cube);
                GameManager.musicCubes.Add(musicCube);
                index++;
            }

            else
            {
                GameObject obstacle = Instantiate(obstacleObject, obstaclePoints[musicCube.posSpawn]);
                obstacle.transform.localPosition = new Vector3(0, 0, GameManager.cubeSpeed * musicCube.timeSpawn);
                obstacle.transform.Rotate(transform.forward, 90 * musicCube.rotation);
                obstacle.transform.parent = cubeParent.transform;
                obstacle.GetComponent<Movement>().mainSlider = mainSlider;
                obstacle.GetComponent<Movement>().musicSlider = musicSlider;
                obstacle.GetComponent<Movement>().id = index;
                GameManager.currentCubes.Add(obstacle);
                GameManager.musicCubes.Add(musicCube);
                index++;
            }
        }
    }

    public void StartSpawn()
    {
        //Lit le fichier XML
        if (System.IO.File.Exists(getPath(musicName)))
        {
            cubeGenerator = XMLOp.Deserialize<CubeGenerator>(getPath(musicName));
            musicCubes = cubeGenerator.musicCubes;
        }

        else
        {
            Debug.Log("No files to load");
        }
        //timer = 0;
        //cubeIndex = 0;
    }

    private string getPath(string musicName)
    {
        //Obtient le path dans lequel lire le fichier XML.
        #if UNITY_EDITOR
            return Application.dataPath + "/CubeGenerators/" + musicName + ".xml";
        #elif UNITY_ANDROID
            return Application.persistentDataPath+musicName+".xml";
        #elif UNITY_IPHONE
            return Application.persistentDataPath+"/"+musicName".xml";
        #else
            return Application.dataPath + "/" + musicName + ".xml";
        #endif
    }
}