using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

//G�re toutes les informations essentielles du jeu (temps de la musique, cubes en jeu, si on est en mode placement ou non...)
//G�re �galement tous les inputs clavier
//Appliqu� sur l'objet GameManager
//Appel� par de nombreux scripts
public class GameManager : MonoBehaviour
{
    public AudioSource audioSource;

    public GameObject Menu, SelectionMenu, AddingMenu, AddingMenuObstacle;

    //public GameObject cubeParent;

    public static List<GameObject> currentCubes = new List<GameObject>(); //Appel� par la classe AddCubes

    public static List<GameObject> selectedCubes = new List<GameObject>(); //Appel� par la classe SeletCubes

    //private CubeGenerator cubeGenerator;

    public static List<MusicCube> musicCubes = new List<MusicCube>(); //Appel� par la classe Spawner

    public static List<MusicCube> copiedCubes = new List<MusicCube>(); //Pas utilis� pour le moment

    public static bool inCubePlacement = false;

    public static bool inCubeAdding = false; //Appel� par la classe AddCubes

    public static float timer; //Appel� par la classe Movement, MusicSlider

    public static float cubeSpeed = 2.5f; //Appel� par la classe Movement, MusicSlider

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Mode placement, on peut ajouter, supprimer et d�placer des cubes
            //On ne peut plus d�placer le curseur, la musique se met en pause et les cubes s'arr�tent
            inCubePlacement = !inCubePlacement;
            if (inCubePlacement)
            {
                audioSource.Pause();
                Menu.SetActive(true);
            }
            else
            {
                //On quitte le mode placement
                audioSource.Play();
                Menu.SetActive(false);
                foreach(GameObject selectedCube in selectedCubes)
                {
                    selectedCube.GetComponent<Renderer>().material.color = Color.black;
                }
                selectedCubes = new List<GameObject>();
            }
        }

        if (Input.GetKeyDown(KeyCode.Delete))
        {
            //Supprime les cubes s�lectionn�s lorsqu'on appuie sur Suppr
            DeleteCube();
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            //Recule les cubes lorsqu'on appuie sur la fl�che arri�re
            MoveBackward();
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            //Avance les cubes lorsqu'on appuie sur la fl�che avant
            MoveForward();
        }

        if (selectedCubes.Count > 0)
        {
            //Active le menu de s�lection lorsqu'au moins un cube est s�lectionn�
            SelectionMenu.SetActive(true);
        }
        else
        {
            SelectionMenu.SetActive(false);
        }

        if (!inCubePlacement)
        {
            timer += Time.deltaTime;
        }
    }

    public void InCubeAdding()
    {
        //Mode ajout de cube : on ne peut plus s�lectionner de cubes
        inCubeAdding = !inCubeAdding;
        AddingMenu.SetActive(inCubeAdding);
        Sort();
    }

    public void InObstacleAdding()
    {
        //Mode ajout de cube : on ne peut plus s�lectionner de cubes
        inCubeAdding = !inCubeAdding;
        AddingMenuObstacle.SetActive(inCubeAdding);
        Sort();
    }

    public void DeleteCube()
    {
        //M�thode permettant de supprimer les cubes � partir de leur id
        //Un peu moche, pas fait au m�me endroit que pour ajouter les cubes (AddCube)
        List<int> removedIds = new List<int>();
        List<MusicCube> removedCubes = new List<MusicCube>();

        foreach (GameObject selectedCube in selectedCubes)
        {
            removedIds.Add(selectedCube.GetComponent<Movement>().id);
            currentCubes.Remove(selectedCube);
            Destroy(selectedCube);
        }

        foreach (MusicCube musicCube in musicCubes)
        {
            if (removedIds.Contains(musicCube.index))
            {
                removedCubes.Add(musicCube);
            }
        }

        foreach (MusicCube musicCube in removedCubes)
        {
            musicCubes.Remove(musicCube);
        }

        Sort();

        selectedCubes = new List<GameObject>();

    }

    public void MoveForward()
    {
        //Le cube arrive plus un dixi�me de seconde plus t�t
        foreach(GameObject selectedCube in selectedCubes)
        {
            selectedCube.transform.position -= transform.forward * 0.1f * cubeSpeed;
            //selectedCube.GetComponent<Movement>().timer += 0.1f;
            selectedCube.GetComponent<Movement>().startPos.z -= GameManager.cubeSpeed * 0.1f;
            int movedId = selectedCube.GetComponent<Movement>().id;
            MusicCube movedCube = musicCubes.Where(c => c.index == movedId).FirstOrDefault();
            movedCube.timeSpawn -= 0.1f;
            //Debug.Log(gameObject.GetComponent<Movement>().timer);
        }

        //Sort();
    }

    public void MoveBackward()
    {
        //Le cube arrive plus un dixi�me de seconde plus tard
        foreach (GameObject selectedCube in selectedCubes)
        {
            selectedCube.transform.position += transform.forward * 0.1f * cubeSpeed;
            //selectedCube.GetComponent<Movement>().timer -= 0.1f;
            selectedCube.GetComponent<Movement>().startPos.z += GameManager.cubeSpeed * 0.1f;
            int movedId = selectedCube.GetComponent<Movement>().id;
            MusicCube movedCube = musicCubes.Where(c => c.index == movedId).FirstOrDefault();
            movedCube.timeSpawn += 0.1f;
        }

        //Sort();
    }

    public void Sort()
    {
        //Trie la liste des cubes de celui qui arrive le plus t�t � celui qui arrive le plus tard
        Debug.Log("Sorting");
        List<MusicCube> sortedMusicCubes = musicCubes.OrderBy(c => c.timeSpawn).ToList();
        musicCubes = sortedMusicCubes;
        List<GameObject> sortedCubes = currentCubes.OrderBy(c => c.transform.position.z).ToList();
        currentCubes = sortedCubes;
        Relabel();
    }

    private void Relabel()
    {
        Debug.Log("Relabeling");
        //Le cube qui arrive en premier est num�rot� 0 et celui qui arrive le dernier n
        for (int i = 0; i < musicCubes.Count; i++)
        {
            musicCubes[i].index = i;
            currentCubes[i].GetComponent<Movement>().id = i;
        }
    }

    public void Save()
    {
        //On enregistre un xml avec le pattern cr��
        CubeGenerator cubeGenerator = new CubeGenerator();
        cubeGenerator.musicName = "Test";
        foreach (MusicCube musicCube in musicCubes)
        {
            cubeGenerator.musicCubes.Add(musicCube);
        }

        XMLOp.Serialize(cubeGenerator, getPath(cubeGenerator.musicName));
    }

    private string getPath(string musicName)
    {
        //Obtient le path dans lequel est enregistr� le fichier XML
        #if UNITY_EDITOR
                    return Application.dataPath + "/CubeGenerators/" + musicName + ".xml";
        #elif UNITY_ANDROID
                    return Application.persistentDataPath+musicName+".xml";
        #elif UNITY_IPHONE
                    return Application.persistentDataPath+"/"+musicName".xml";
        #else
                return Application.dataPath + "/" + musicName + ".xml";
        #endif
    }
}
