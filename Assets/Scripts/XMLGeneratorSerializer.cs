using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Exemple de script permettant de g�n�rer une chor�graphie
//Pas appel�
public class XMLGeneratorSerializer : MonoBehaviour
{
    private void Start()
    {
        CubeGenerator cubeGenerator = new CubeGenerator();
        cubeGenerator.musicName = "PallaVoy";
        MusicCube musicCube1 = new MusicCube();
        musicCube1.color = 1;
        musicCube1.posSpawn = 0;
        musicCube1.timeSpawn = 0;
        musicCube1.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube1);

        MusicCube musicCube2 = new MusicCube();
        musicCube2.color = 0;
        musicCube2.posSpawn = 1;
        musicCube2.timeSpawn = 0.29f;
        musicCube2.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube2);

        MusicCube musicCube3 = new MusicCube();
        musicCube3.color = 1;
        musicCube3.posSpawn = 0;
        musicCube3.timeSpawn = 2.18f;
        musicCube3.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube3);

        MusicCube musicCube4 = new MusicCube();
        musicCube4.color = 0;
        musicCube4.posSpawn = 1;
        musicCube4.timeSpawn = 0.32f;
        musicCube4.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube4);

        MusicCube musicCube5 = new MusicCube();
        musicCube5.color = 1;
        musicCube5.posSpawn = 0;
        musicCube5.timeSpawn = 2.16f;
        musicCube5.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube5);

        MusicCube musicCube5_5 = new MusicCube();
        musicCube5_5.color = 0;
        musicCube5_5.posSpawn = 1;
        musicCube5_5.timeSpawn = 0.3f;
        musicCube5_5.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube5_5);

        MusicCube musicCube6 = new MusicCube();
        musicCube6.color = 1;
        musicCube6.posSpawn = 0;
        musicCube6.timeSpawn = 2.25f;
        musicCube6.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube6);

        MusicCube musicCube7 = new MusicCube();
        musicCube7.color = 0;
        musicCube7.posSpawn = 1;
        musicCube7.timeSpawn = 0.3f;
        musicCube7.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube7);

        MusicCube musicCube8 = new MusicCube();
        musicCube8.color = 1;
        musicCube8.posSpawn = 0;
        musicCube8.timeSpawn = 2.17f;
        musicCube8.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube8);

        MusicCube musicCube9 = new MusicCube();
        musicCube9.color = 0;
        musicCube9.posSpawn = 1;
        musicCube9.timeSpawn = 0.29f;
        musicCube9.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube9);

        MusicCube musicCube10 = new MusicCube();
        musicCube10.color = 1;
        musicCube10.posSpawn = 0;
        musicCube10.timeSpawn = 2.17f;
        musicCube10.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube10);

        MusicCube musicCube11 = new MusicCube();
        musicCube11.color = 0;
        musicCube11.posSpawn = 1;
        musicCube11.timeSpawn = 0.29f;
        musicCube11.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube11);

        MusicCube musicCube12 = new MusicCube();
        musicCube12.color = 1;
        musicCube12.posSpawn = 0;
        musicCube12.timeSpawn = 2.17f;
        musicCube12.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube12);

        MusicCube musicCube13 = new MusicCube();
        musicCube13.color = 0;
        musicCube13.posSpawn = 1;
        musicCube13.timeSpawn = 0.29f;
        musicCube13.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube13);

        MusicCube musicCube14 = new MusicCube();
        musicCube14.color = 1;
        musicCube14.posSpawn = 0;
        musicCube14.timeSpawn = 2.17f;
        musicCube14.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube14);

        MusicCube musicCube15 = new MusicCube();
        musicCube15.color = 0;
        musicCube15.posSpawn = 1;
        musicCube15.timeSpawn = 0.29f;
        musicCube15.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube15);

        MusicCube musicCube16 = new MusicCube();
        musicCube16.color = 1;
        musicCube16.posSpawn = 2;
        musicCube16.timeSpawn = 0.5f;
        musicCube16.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube16);

        MusicCube musicCube17 = new MusicCube();
        musicCube17.color = 0;
        musicCube17.posSpawn = 3;
        musicCube17.timeSpawn = 0.24f;
        musicCube17.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube17);

        //Palla'Voy

        MusicCube musicCube18 = new MusicCube();
        musicCube18.color = 0;
        musicCube18.posSpawn = 3;
        musicCube18.timeSpawn = 1.31f;
        musicCube18.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube18);

        MusicCube musicCube19 = new MusicCube();
        musicCube19.color = 1;
        musicCube19.posSpawn = 2;
        musicCube19.timeSpawn = 0.17f;
        musicCube19.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube19);

        MusicCube musicCube20 = new MusicCube();
        musicCube20.color = 1;
        musicCube20.posSpawn = 0;
        musicCube20.timeSpawn = 0.25f;
        musicCube20.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube20);

        MusicCube musicCube21 = new MusicCube();
        musicCube21.color = 1;
        musicCube21.posSpawn = 2;
        musicCube21.timeSpawn = 2.09f;
        musicCube21.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube21);

        MusicCube musicCube22 = new MusicCube();
        musicCube22.color = 0;
        musicCube22.posSpawn = 3;
        musicCube22.timeSpawn = 0.17f;
        musicCube22.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube22);

        MusicCube musicCube23 = new MusicCube();
        musicCube23.color = 0;
        musicCube23.posSpawn = 1;
        musicCube23.timeSpawn = 0.25f;
        musicCube23.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube23);


        //Musica y alcohol


        MusicCube musicCube24 = new MusicCube();
        musicCube24.color = 0;
        musicCube24.posSpawn = 3;
        musicCube24.timeSpawn = 4.57f;
        musicCube24.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube24);

        MusicCube musicCube25 = new MusicCube();
        musicCube25.color = 1;
        musicCube25.posSpawn = 2;
        musicCube25.timeSpawn = 0.17f;
        musicCube25.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube25);

        MusicCube musicCube26 = new MusicCube();
        musicCube26.color = 1;
        musicCube26.posSpawn = 0;
        musicCube26.timeSpawn = 0.25f;
        musicCube26.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube26);

        MusicCube musicCube27 = new MusicCube();
        musicCube27.color = 0;
        musicCube27.posSpawn = 3;
        musicCube27.timeSpawn = 1.96f;
        musicCube27.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube27);

        MusicCube musicCube28 = new MusicCube();
        musicCube28.color = 1;
        musicCube28.posSpawn = 2;
        musicCube28.timeSpawn = 0.17f;
        musicCube28.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube28);

        MusicCube musicCube29 = new MusicCube();
        musicCube29.color = 1;
        musicCube29.posSpawn = 0;
        musicCube29.timeSpawn = 0.25f;
        musicCube29.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube29);

        MusicCube musicCube30 = new MusicCube();
        musicCube30.color = 1;
        musicCube30.posSpawn = 2;
        musicCube30.timeSpawn = 1.96f;
        musicCube30.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube30);

        MusicCube musicCube31 = new MusicCube();
        musicCube31.color = 0;
        musicCube31.posSpawn = 3;
        musicCube31.timeSpawn = 0.17f;
        musicCube31.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube31);

        MusicCube musicCube32 = new MusicCube();
        musicCube32.color = 0;
        musicCube32.posSpawn = 1;
        musicCube32.timeSpawn = 0.25f;
        musicCube32.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube32);

        MusicCube musicCube33 = new MusicCube();
        musicCube33.color = 0;
        musicCube33.posSpawn = 3;
        musicCube33.timeSpawn = 1.96f;
        musicCube33.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube33);

        MusicCube musicCube34 = new MusicCube();
        musicCube34.color = 1;
        musicCube34.posSpawn = 2;
        musicCube34.timeSpawn = 0.17f;
        musicCube34.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube34);

        MusicCube musicCube35 = new MusicCube();
        musicCube35.color = 1;
        musicCube35.posSpawn = 0;
        musicCube35.timeSpawn = 0.25f;
        musicCube35.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube35);

        //Palla me voy

        MusicCube musicCube36 = new MusicCube();
        musicCube36.color = 1;
        musicCube36.posSpawn = 2;
        musicCube36.timeSpawn = 2.1f;
        musicCube36.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube36);

        MusicCube musicCube37 = new MusicCube();
        musicCube37.color = 0;
        musicCube37.posSpawn = 3;
        musicCube37.timeSpawn = 0.24f;
        musicCube37.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube37);

        MusicCube musicCube38 = new MusicCube();
        musicCube38.color = 0;
        musicCube38.posSpawn = 1;
        musicCube38.timeSpawn = 0.34f;
        musicCube38.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube38);

        MusicCube musicCube39 = new MusicCube();
        musicCube39.color = 0;
        musicCube39.posSpawn = 3;
        musicCube39.timeSpawn = 2.1f;
        musicCube39.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube39);

        MusicCube musicCube40 = new MusicCube();
        musicCube40.color = 1;
        musicCube40.posSpawn = 2;
        musicCube40.timeSpawn = 0.17f;
        musicCube40.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube40);

        MusicCube musicCube41 = new MusicCube();
        musicCube41.color = 1;
        musicCube41.posSpawn = 0;
        musicCube41.timeSpawn = 0.25f;
        musicCube41.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube41);

        MusicCube musicCube42 = new MusicCube();
        musicCube42.color = 1;
        musicCube42.posSpawn = 2;
        musicCube42.timeSpawn = 2.1f;
        musicCube42.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube42);

        MusicCube musicCube43 = new MusicCube();
        musicCube43.color = 0;
        musicCube43.posSpawn = 3;
        musicCube43.timeSpawn = 0.17f;
        musicCube43.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube43);

        MusicCube musicCube44 = new MusicCube();
        musicCube44.color = 0;
        musicCube44.posSpawn = 1;
        musicCube44.timeSpawn = 0.25f;
        musicCube44.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube44);

        MusicCube musicCube45 = new MusicCube();
        musicCube45.color = 0;
        musicCube45.posSpawn = 3;
        musicCube45.timeSpawn = 2.1f;
        musicCube45.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube45);

        MusicCube musicCube46 = new MusicCube();
        musicCube46.color = 1;
        musicCube46.posSpawn = 2;
        musicCube46.timeSpawn = 0.17f;
        musicCube46.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube46);

        MusicCube musicCube47 = new MusicCube();
        musicCube47.color = 1;
        musicCube47.posSpawn = 0;
        musicCube47.timeSpawn = 0.25f;
        musicCube47.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube47);

        MusicCube musicCube48 = new MusicCube();
        musicCube48.color = 1;
        musicCube48.posSpawn = 2;
        musicCube48.timeSpawn = 2.1f;
        musicCube48.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube48);

        MusicCube musicCube49 = new MusicCube();
        musicCube49.color = 0;
        musicCube49.posSpawn = 3;
        musicCube49.timeSpawn = 0.17f;
        musicCube49.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube49);

        MusicCube musicCube50 = new MusicCube();
        musicCube50.color = 0;
        musicCube50.posSpawn = 1;
        musicCube50.timeSpawn = 0.25f;
        musicCube50.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube50);

        //La calle me llama

        MusicCube musicCube51 = new MusicCube();
        musicCube51.color = 0;
        musicCube51.posSpawn = 1;
        musicCube51.timeSpawn = 1.9f;
        musicCube51.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube51);

        MusicCube musicCube52 = new MusicCube();
        musicCube52.color = 1;
        musicCube52.posSpawn = 0;
        musicCube52.timeSpawn = 0.24f;
        musicCube52.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube52);

        MusicCube musicCube53 = new MusicCube();
        musicCube53.color = 0;
        musicCube53.posSpawn = 0;
        musicCube53.timeSpawn = 0.25f;
        musicCube53.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube53);

        MusicCube musicCube54 = new MusicCube();
        musicCube54.color = 1;
        musicCube54.posSpawn = 0;
        musicCube54.timeSpawn = 2.1f;
        musicCube54.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube54);

        MusicCube musicCube55 = new MusicCube();
        musicCube55.color = 0;
        musicCube55.posSpawn = 1;
        musicCube55.timeSpawn = 0.22f;
        musicCube55.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube55);

        MusicCube musicCube56 = new MusicCube();
        musicCube56.color = 1;
        musicCube56.posSpawn = 1;
        musicCube56.timeSpawn = 0.26f;
        musicCube56.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube56);

        //Instru (rajouter des cubes)

        MusicCube Cube1 = new MusicCube();
        Cube1.color = 0;
        Cube1.posSpawn = 1;
        Cube1.timeSpawn = 1.7f;
        Cube1.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube1);

        MusicCube Cube2 = new MusicCube();
        Cube2.color = 1;
        Cube2.posSpawn = 0;
        Cube2.timeSpawn = 0.8f;
        Cube2.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube2);

        MusicCube Cube3 = new MusicCube();
        Cube3.color = 0;
        Cube3.posSpawn = 3;
        Cube3.timeSpawn = 1.7f;
        Cube3.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube3);

        MusicCube Cube4 = new MusicCube();
        Cube4.color = 1;
        Cube4.posSpawn = 2;
        Cube4.timeSpawn = 0.8f;
        Cube4.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube4);

        MusicCube Cube5 = new MusicCube();
        Cube5.color = 0;
        Cube5.posSpawn = 1;
        Cube5.timeSpawn = 1.7f;
        Cube5.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube5);

        MusicCube Cube6 = new MusicCube();
        Cube6.color = 1;
        Cube6.posSpawn = 0;
        Cube6.timeSpawn = 0.8f;
        Cube6.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube6);

        MusicCube Cube7 = new MusicCube();
        Cube7.color = 0;
        Cube7.posSpawn = 3;
        Cube7.timeSpawn = 1.7f;
        Cube7.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube7);

        MusicCube Cube8 = new MusicCube();
        Cube8.color = 1;
        Cube8.posSpawn = 2;
        Cube8.timeSpawn = 0.8f;
        Cube8.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube8);

        //Trompettes

        //cubeGenerator.musicCubes.Add(Cube5);

        MusicCube Cube9 = new MusicCube();
        Cube9.color = 1;
        Cube9.posSpawn = 0;
        Cube9.timeSpawn = 2.45f; //2.45f
        Cube9.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube9);

        MusicCube Cube9_5 = new MusicCube();
        Cube9_5.color = 0;
        Cube9_5.posSpawn = 2;
        Cube9_5.timeSpawn = 0;
        Cube9_5.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube9_5);

        /*MusicCube Cube9_7 = new MusicCube();
        Cube9_7.color = 0;
        Cube9_7.posSpawn = 0;
        Cube9_7.timeSpawn = 1.7f;
        Cube9_7.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube9_7);*/

        MusicCube Cube10 = new MusicCube();
        Cube10.color = 0;
        Cube10.posSpawn = 1;
        Cube10.timeSpawn = 2.45f;
        Cube10.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube10);

        MusicCube Cube10_5 = new MusicCube();
        Cube10_5.color = 1;
        Cube10_5.posSpawn = 3;
        Cube10_5.timeSpawn = 0;
        Cube10_5.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube10_5);

        MusicCube Cube11 = new MusicCube();
        Cube11.color = 1;
        Cube11.posSpawn = 2;
        Cube11.timeSpawn = 2.45f;
        Cube11.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube11);

        MusicCube Cube11_5 = new MusicCube();
        Cube11_5.color = 0;
        Cube11_5.posSpawn = 0;
        Cube11_5.timeSpawn = 0;
        Cube11_5.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube11_5);

        MusicCube Cube12 = new MusicCube();
        Cube12.color = 0;
        Cube12.posSpawn = 3;
        Cube12.timeSpawn = 2.45f;
        Cube12.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube12);

        MusicCube Cube12_5 = new MusicCube();
        Cube12_5.color = 1;
        Cube12_5.posSpawn = 1;
        Cube12_5.timeSpawn = 0;
        Cube12_5.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube12_5);

        MusicCube Cube13 = new MusicCube();
        Cube13.color = 1;
        Cube13.posSpawn = 0;
        Cube13.timeSpawn = 1.7f;
        Cube13.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube13);

        MusicCube Cube14 = new MusicCube();
        Cube14.color = 0;
        Cube14.posSpawn = 1;
        Cube14.timeSpawn = 0.8f;
        Cube14.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube14);

        MusicCube Cube15 = new MusicCube();
        Cube15.color = 1;
        Cube15.posSpawn = 2;
        Cube15.timeSpawn = 1.7f;
        Cube15.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube15);

        MusicCube Cube16 = new MusicCube();
        Cube16.color = 0;
        Cube16.posSpawn = 3;
        Cube16.timeSpawn = 0.8f;
        Cube16.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube16);

        MusicCube Cube17 = new MusicCube();
        Cube17.color = 1;
        Cube17.posSpawn = 0;
        Cube17.timeSpawn = 1.7f;
        Cube17.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube17);

        MusicCube Cube18 = new MusicCube();
        Cube18.color = 0;
        Cube18.posSpawn = 1;
        Cube18.timeSpawn = 0.8f;
        Cube18.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube18);

        MusicCube Cube19 = new MusicCube();
        Cube19.color = 1;
        Cube19.posSpawn = 2;
        Cube19.timeSpawn = 1.7f;
        Cube19.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube19);

        MusicCube Cube20 = new MusicCube();
        Cube20.color = 0;
        Cube20.posSpawn = 3;
        Cube20.timeSpawn = 0.8f;
        Cube20.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube20);

        MusicCube Cube21 = new MusicCube();
        Cube21.color = 0;
        Cube21.posSpawn = 2;
        Cube21.timeSpawn = 2.45f;
        Cube21.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube21);

        MusicCube Cube21_5 = new MusicCube();
        Cube21_5.color = 1;
        Cube21_5.posSpawn = 0;
        Cube21_5.timeSpawn = 0;
        Cube21_5.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube21_5);

        MusicCube Cube22 = new MusicCube();
        Cube22.color = 0;
        Cube22.posSpawn = 3;
        Cube22.timeSpawn = 2.45f;
        Cube22.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube22);

        MusicCube Cube22_5 = new MusicCube();
        Cube22_5.color = 1;
        Cube22_5.posSpawn = 1;
        Cube22_5.timeSpawn = 0;
        Cube22_5.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube22_5);

        MusicCube Cube23 = new MusicCube();
        Cube23.color = 0;
        Cube23.posSpawn = 2;
        Cube23.timeSpawn = 2.45f;
        Cube23.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube23);

        MusicCube Cube23_5 = new MusicCube();
        Cube23_5.color = 1;
        Cube23_5.posSpawn = 0;
        Cube23_5.timeSpawn = 0;
        Cube23_5.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube23_5);

        MusicCube Cube24 = new MusicCube();
        Cube24.color = 0;
        Cube24.posSpawn = 3;
        Cube24.timeSpawn = 2.45f;
        Cube24.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube24);

        MusicCube Cube24_5 = new MusicCube();
        Cube24_5.color = 1;
        Cube24_5.posSpawn = 1;
        Cube24_5.timeSpawn = 0;
        Cube24_5.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube24_5);
        //

        MusicCube musicCube57 = new MusicCube();
        musicCube57.color = 0;
        musicCube57.posSpawn = 3;
        musicCube57.timeSpawn = 2.3f;
        musicCube57.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube57);

        MusicCube musicCube58 = new MusicCube();
        musicCube58.color = 1;
        musicCube58.posSpawn = 2;
        musicCube58.timeSpawn = 0.17f;
        musicCube58.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube58);

        MusicCube musicCube59 = new MusicCube();
        musicCube59.color = 1;
        musicCube59.posSpawn = 0;
        musicCube59.timeSpawn = 0.25f;
        musicCube59.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube59);

        MusicCube musicCube60 = new MusicCube();
        musicCube60.color = 1;
        musicCube60.posSpawn = 2;
        musicCube60.timeSpawn = 2.1f;
        musicCube60.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube60);

        MusicCube musicCube61 = new MusicCube();
        musicCube61.color = 0;
        musicCube61.posSpawn = 3;
        musicCube61.timeSpawn = 0.17f;
        musicCube61.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube61);

        MusicCube musicCube62 = new MusicCube();
        musicCube62.color = 0;
        musicCube62.posSpawn = 1;
        musicCube62.timeSpawn = 0.25f;
        musicCube62.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube62);

        //Musica y alcohol

        MusicCube musicCube63 = new MusicCube();
        musicCube63.color = 0;
        musicCube63.posSpawn = 3;
        musicCube63.timeSpawn = 4.57f;
        musicCube63.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube63);

        MusicCube musicCube64 = new MusicCube();
        musicCube64.color = 1;
        musicCube64.posSpawn = 2;
        musicCube64.timeSpawn = 0.17f;
        musicCube64.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube64);

        MusicCube musicCube65 = new MusicCube();
        musicCube65.color = 1;
        musicCube65.posSpawn = 0;
        musicCube65.timeSpawn = 0.25f;
        musicCube65.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube65);

        MusicCube musicCube66 = new MusicCube();
        musicCube66.color = 1;
        musicCube66.posSpawn = 2;
        musicCube66.timeSpawn = 1.9f;
        musicCube66.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube66);

        MusicCube musicCube67 = new MusicCube();
        musicCube67.color = 0;
        musicCube67.posSpawn = 3;
        musicCube67.timeSpawn = 0.17f;
        musicCube67.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube67);
        
        MusicCube musicCube68 = new MusicCube();
        musicCube68.color = 0;
        musicCube68.posSpawn = 1;
        musicCube68.timeSpawn = 0.25f;
        musicCube68.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube68);

        MusicCube musicCube69 = new MusicCube();
        musicCube69.color = 0;
        musicCube69.posSpawn = 3;
        musicCube69.timeSpawn = 1.9f;
        musicCube69.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube69);

        MusicCube musicCube70 = new MusicCube();
        musicCube70.color = 1;
        musicCube70.posSpawn = 2;
        musicCube70.timeSpawn = 0.17f;
        musicCube70.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube70);

        MusicCube musicCube71 = new MusicCube();
        musicCube71.color = 1;
        musicCube71.posSpawn = 0;
        musicCube71.timeSpawn = 0.25f;
        musicCube71.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube71);

        //Musica y alcohol

        MusicCube musicCube72 = new MusicCube();
        musicCube72.color = 1;
        musicCube72.posSpawn = 2;
        musicCube72.timeSpawn = 4.57f;
        musicCube72.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube72);

        MusicCube musicCube73 = new MusicCube();
        musicCube73.color = 0;
        musicCube73.posSpawn = 3;
        musicCube73.timeSpawn = 0.17f;
        musicCube73.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube73);

        MusicCube musicCube74 = new MusicCube();
        musicCube74.color = 0;
        musicCube74.posSpawn = 1;
        musicCube74.timeSpawn = 0.25f;
        musicCube74.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube74);

        MusicCube musicCube75 = new MusicCube();
        musicCube75.color = 0;
        musicCube75.posSpawn = 3;
        musicCube75.timeSpawn = 2.1f;
        musicCube75.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube75);

        MusicCube musicCube76 = new MusicCube();
        musicCube76.color = 1;
        musicCube76.posSpawn = 2;
        musicCube76.timeSpawn = 0.17f;
        musicCube76.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube76);

        MusicCube musicCube77 = new MusicCube();
        musicCube77.color = 1;
        musicCube77.posSpawn = 0;
        musicCube77.timeSpawn = 0.25f;
        musicCube77.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube77);

        MusicCube musicCube78 = new MusicCube();
        musicCube78.color = 1;
        musicCube78.posSpawn = 2;
        musicCube78.timeSpawn = 2.1f;
        musicCube78.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube78);

        MusicCube musicCube79 = new MusicCube();
        musicCube79.color = 0;
        musicCube79.posSpawn = 3;
        musicCube79.timeSpawn = 0.17f;
        musicCube79.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube79);

        MusicCube musicCube80 = new MusicCube();
        musicCube80.color = 0;
        musicCube80.posSpawn = 1;
        musicCube80.timeSpawn = 0.25f;
        musicCube80.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube80);

        MusicCube musicCube81 = new MusicCube();
        musicCube81.color = 0;
        musicCube81.posSpawn = 3;
        musicCube81.timeSpawn = 2.1f;
        musicCube81.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube81);

        MusicCube musicCube82 = new MusicCube();
        musicCube82.color = 1;
        musicCube82.posSpawn = 2;
        musicCube82.timeSpawn = 0.17f;
        musicCube82.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube82);

        MusicCube musicCube83 = new MusicCube();
        musicCube83.color = 1;
        musicCube83.posSpawn = 0;
        musicCube83.timeSpawn = 0.25f;
        musicCube83.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube83);

        MusicCube musicCube84 = new MusicCube();
        musicCube84.color = 1;
        musicCube84.posSpawn = 2;
        musicCube84.timeSpawn = 2.1f;
        musicCube84.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube84);

        MusicCube musicCube85 = new MusicCube();
        musicCube85.color = 0;
        musicCube85.posSpawn = 3;
        musicCube85.timeSpawn = 0.17f;
        musicCube85.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube85);

        MusicCube musicCube86 = new MusicCube();
        musicCube86.color = 0;
        musicCube86.posSpawn = 1;
        musicCube86.timeSpawn = 0.25f;
        musicCube86.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube86);

        MusicCube musicCube87 = new MusicCube();
        musicCube87.color = 0;
        musicCube87.posSpawn = 3;
        musicCube87.timeSpawn = 2.1f;
        musicCube87.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube87);

        MusicCube musicCube88 = new MusicCube();
        musicCube88.color = 1;
        musicCube88.posSpawn = 2;
        musicCube88.timeSpawn = 0.17f;
        musicCube88.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube88);

        MusicCube musicCube89 = new MusicCube();
        musicCube89.color = 1;
        musicCube89.posSpawn = 0;
        musicCube89.timeSpawn = 0.25f;
        musicCube89.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube89);

        MusicCube musicCube90 = new MusicCube();
        musicCube90.color = 1;
        musicCube90.posSpawn = 2;
        musicCube90.timeSpawn = 2;
        musicCube90.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube90);

        MusicCube musicCube91 = new MusicCube();
        musicCube91.color = 0;
        musicCube91.posSpawn = 3;
        musicCube91.timeSpawn = 0.17f;
        musicCube91.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube91);

        MusicCube musicCube92 = new MusicCube();
        musicCube92.color = 0;
        musicCube92.posSpawn = 1;
        musicCube92.timeSpawn = 0.25f;
        musicCube92.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube92);

        MusicCube musicCube93 = new MusicCube();
        musicCube93.color = 0;
        musicCube93.posSpawn = 3;
        musicCube93.timeSpawn = 2.1f;
        musicCube93.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube93);

        MusicCube musicCube94 = new MusicCube();
        musicCube94.color = 1;
        musicCube94.posSpawn = 2;
        musicCube94.timeSpawn = 0.17f;
        musicCube94.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube94);

        MusicCube musicCube95 = new MusicCube();
        musicCube95.color = 1;
        musicCube95.posSpawn = 0;
        musicCube95.timeSpawn = 0.25f;
        musicCube95.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube95);

        MusicCube musicCube96 = new MusicCube();
        musicCube96.color = 1;
        musicCube96.posSpawn = 2;
        musicCube96.timeSpawn = 2;
        musicCube96.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube96);

        MusicCube musicCube97 = new MusicCube();
        musicCube97.color = 0;
        musicCube97.posSpawn = 3;
        musicCube97.timeSpawn = 0.17f;
        musicCube97.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube97);

        MusicCube musicCube98 = new MusicCube();
        musicCube98.color = 0;
        musicCube98.posSpawn = 1;
        musicCube98.timeSpawn = 0.25f;
        musicCube98.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube98);

        //Instru 2 : rajouter des cubes

        MusicCube Cube25 = new MusicCube();
        Cube25.color = 1;
        Cube25.posSpawn = 0;
        Cube25.timeSpawn = 11.5f;
        Cube25.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube25);

        MusicCube Cube26 = new MusicCube();
        Cube26.color = 0;
        Cube26.posSpawn = 1;
        Cube26.timeSpawn = 0.6f;
        Cube26.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube26);

        MusicCube Cube27 = new MusicCube();
        Cube27.color = 1;
        Cube27.posSpawn = 2;
        Cube27.timeSpawn = 0.6f;
        Cube27.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube27);

        MusicCube Cube28 = new MusicCube();
        Cube28.color = 0;
        Cube28.posSpawn = 3;
        Cube28.timeSpawn = 0.6f;
        Cube28.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube28);

        MusicCube Cube29 = new MusicCube();
        Cube29.color = 1;
        Cube29.posSpawn = 0;
        Cube29.timeSpawn = 0.6f;
        Cube29.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube29);

        MusicCube Cube30 = new MusicCube();
        Cube30.color = 0;
        Cube30.posSpawn = 1;
        Cube30.timeSpawn = 0.6f;
        Cube30.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube30);

        MusicCube Cube31 = new MusicCube();
        Cube31.color = 1;
        Cube31.posSpawn = 2;
        Cube31.timeSpawn = 0.6f;
        Cube31.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube31);

        MusicCube Cube32 = new MusicCube();
        Cube32.color = 0;
        Cube32.posSpawn = 3;
        Cube32.timeSpawn = 0.6f;
        Cube32.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube32);

        MusicCube Cube33 = new MusicCube();
        Cube33.color = 1;
        Cube33.posSpawn = 0;
        Cube33.timeSpawn = 0.6f;
        Cube33.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube33);

        MusicCube Cube34 = new MusicCube();
        Cube34.color = 0;
        Cube34.posSpawn = 1;
        Cube34.timeSpawn = 0.6f;
        Cube34.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube34);

        MusicCube Cube35 = new MusicCube();
        Cube35.color = 1;
        Cube35.posSpawn = 2;
        Cube35.timeSpawn = 0.6f;
        Cube35.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube35);

        MusicCube Cube36 = new MusicCube();
        Cube36.color = 0;
        Cube36.posSpawn = 3;
        Cube36.timeSpawn = 0.6f;
        Cube36.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube36);

        MusicCube Cube37 = new MusicCube();
        Cube37.color = 1;
        Cube37.posSpawn = 0;
        Cube37.timeSpawn = 0.6f;
        Cube37.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube37);

        MusicCube Cube38 = new MusicCube();
        Cube38.color = 0;
        Cube38.posSpawn = 1;
        Cube38.timeSpawn = 0.3f;
        Cube38.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube38);

        MusicCube Cube39 = new MusicCube();
        Cube39.color = 1;
        Cube39.posSpawn = 2;
        Cube39.timeSpawn = 0.3f;
        Cube39.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube39);

        /*MusicCube Cube40 = new MusicCube();
        Cube40.color = 0;
        Cube40.posSpawn = 2;
        Cube40.timeSpawn = 0.15f;
        Cube40.rotation = 2;
        cubeGenerator.musicCubes.Add(Cube40);*/

        MusicCube Cube41 = new MusicCube();
        Cube41.color = 0;
        Cube41.posSpawn = 3;
        Cube41.timeSpawn = 0.3f;
        Cube41.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube41);

        /*MusicCube Cube42 = new MusicCube();
        Cube42.color = 0;
        Cube42.posSpawn = 2;
        Cube42.timeSpawn = 0.15f;
        Cube42.rotation = 2;
        cubeGenerator.musicCubes.Add(Cube42);*/

        MusicCube Cube43 = new MusicCube();
        Cube43.color = 1;
        Cube43.posSpawn = 0;
        Cube43.timeSpawn = 0.3f;
        Cube43.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube43);

        /*MusicCube Cube44 = new MusicCube();
        Cube44.color = 0;
        Cube44.posSpawn = 2;
        Cube44.timeSpawn = 0.15f;
        Cube44.rotation = 2;
        cubeGenerator.musicCubes.Add(Cube44);*/

        MusicCube Cube45 = new MusicCube();
        Cube45.color = 0;
        Cube45.posSpawn = 1;
        Cube45.timeSpawn = 0.35f;
        Cube45.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube45);

        MusicCube Cube46 = new MusicCube();
        Cube46.color = 1;
        Cube46.posSpawn = 2;
        Cube46.timeSpawn = 0.3f;
        Cube46.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube46);

        MusicCube Cube47 = new MusicCube();
        Cube47.color = 0;
        Cube47.posSpawn = 3;
        Cube47.timeSpawn = 0.4f;
        Cube47.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube47);

        MusicCube Cube48 = new MusicCube();
        Cube48.color = 1;
        Cube48.posSpawn = 0;
        Cube48.timeSpawn = 0.75f;
        Cube48.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube48);

        MusicCube Cube49 = new MusicCube();
        Cube49.color = 0;
        Cube49.posSpawn = 1;
        Cube49.timeSpawn = 0.9f;
        Cube49.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube49);

        MusicCube Cube50 = new MusicCube();
        Cube50.color = 1;
        Cube50.posSpawn = 2;
        Cube50.timeSpawn = 1.7f;
        Cube50.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube50);

        MusicCube Cube51 = new MusicCube();
        Cube51.color = 0;
        Cube51.posSpawn = 3;
        Cube51.timeSpawn = 0.8f;
        Cube51.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube51);

        MusicCube Cube52 = new MusicCube();
        Cube52.color = 1;
        Cube52.posSpawn = 0;
        Cube52.timeSpawn = 1.7f;
        Cube52.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube52);

        MusicCube Cube53 = new MusicCube();
        Cube53.color = 0;
        Cube53.posSpawn = 1;
        Cube53.timeSpawn = 0.8f;
        Cube53.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube53);

        MusicCube Cube54 = new MusicCube();
        Cube54.color = 1;
        Cube54.posSpawn = 2;
        Cube54.timeSpawn = 1.7f;
        Cube54.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube54);

        MusicCube Cube55 = new MusicCube();
        Cube55.color = 0;
        Cube55.posSpawn = 3;
        Cube55.timeSpawn = 0.8f;
        Cube55.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube55);

        MusicCube Cube56 = new MusicCube();
        Cube56.color = 1;
        Cube56.posSpawn = 0;
        Cube56.timeSpawn = 2.45f;
        Cube56.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube56);

        MusicCube Cube56_5 = new MusicCube();
        Cube56_5.color = 0;
        Cube56_5.posSpawn = 2;
        Cube56_5.timeSpawn = 0;
        Cube56_5.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube56_5);

        MusicCube Cube57 = new MusicCube();
        Cube57.color = 0;
        Cube57.posSpawn = 1;
        Cube57.timeSpawn = 2.45f;
        Cube57.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube57);

        MusicCube Cube57_5 = new MusicCube();
        Cube57_5.color = 1;
        Cube57_5.posSpawn = 3;
        Cube57_5.timeSpawn = 0;
        Cube57_5.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube57_5);

        MusicCube Cube58 = new MusicCube();
        Cube58.color = 1;
        Cube58.posSpawn = 2;
        Cube58.timeSpawn = 2.45f;
        Cube58.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube58);

        MusicCube Cube58_5 = new MusicCube();
        Cube58_5.color = 0;
        Cube58_5.posSpawn = 0;
        Cube58_5.timeSpawn = 0;
        Cube58_5.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube58_5);

        MusicCube Cube59 = new MusicCube();
        Cube59.color = 0;
        Cube59.posSpawn = 3;
        Cube59.timeSpawn = 2.45f;
        Cube59.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube59);

        MusicCube Cube59_5 = new MusicCube();
        Cube59_5.color = 1;
        Cube59_5.posSpawn = 1;
        Cube59_5.timeSpawn = 0;
        Cube59_5.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube59_5);

        MusicCube Cube60 = new MusicCube();
        Cube60.color = 1;
        Cube60.posSpawn = 0;
        Cube60.timeSpawn = 1.7f;
        Cube60.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube60);

        MusicCube Cube61 = new MusicCube();
        Cube61.color = 0;
        Cube61.posSpawn = 1;
        Cube61.timeSpawn = 0.8f;
        Cube61.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube61);

        MusicCube Cube62 = new MusicCube();
        Cube62.color = 1;
        Cube62.posSpawn = 2;
        Cube62.timeSpawn = 1.7f;
        Cube62.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube62);

        MusicCube Cube63 = new MusicCube();
        Cube63.color = 0;
        Cube63.posSpawn = 3;
        Cube63.timeSpawn = 0.8f;
        Cube63.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube63);

        MusicCube Cube64 = new MusicCube();
        Cube64.color = 1;
        Cube64.posSpawn = 0;
        Cube64.timeSpawn = 1.7f;
        Cube64.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube64);

        MusicCube Cube65 = new MusicCube();
        Cube65.color = 0;
        Cube65.posSpawn = 1;
        Cube65.timeSpawn = 0.8f;
        Cube65.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube65);

        MusicCube Cube66 = new MusicCube();
        Cube66.color = 1;
        Cube66.posSpawn = 2;
        Cube66.timeSpawn = 1.7f;
        Cube66.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube66);

        MusicCube Cube67 = new MusicCube();
        Cube67.color = 0;
        Cube67.posSpawn = 3;
        Cube67.timeSpawn = 0.8f;
        Cube67.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube67);

        MusicCube Cube68 = new MusicCube();
        Cube68.color = 1;
        Cube68.posSpawn = 0;
        Cube68.timeSpawn = 2.45f;
        Cube68.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube68);

        MusicCube Cube68_5 = new MusicCube();
        Cube68_5.color = 0;
        Cube68_5.posSpawn = 2;
        Cube68_5.timeSpawn = 0;
        Cube68_5.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube68_5);

        MusicCube Cube69 = new MusicCube();
        Cube69.color = 0;
        Cube69.posSpawn = 1;
        Cube69.timeSpawn = 2.45f;
        Cube69.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube69);

        MusicCube Cube69_5 = new MusicCube();
        Cube69_5.color = 1;
        Cube69_5.posSpawn = 3;
        Cube69_5.timeSpawn = 0;
        Cube69_5.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube69_5);

        MusicCube Cube70 = new MusicCube();
        Cube70.color = 1;
        Cube70.posSpawn = 0;
        Cube70.timeSpawn = 2.45f;
        Cube70.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube70);

        MusicCube Cube70_5 = new MusicCube();
        Cube70_5.color = 0;
        Cube70_5.posSpawn = 2;
        Cube70_5.timeSpawn = 0;
        Cube70_5.rotation = 1;
        cubeGenerator.musicCubes.Add(Cube70_5);

        MusicCube Cube71 = new MusicCube();
        Cube71.color = 0;
        Cube71.posSpawn = 1;
        Cube71.timeSpawn = 2.45f;
        Cube71.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube71);

        MusicCube Cube71_5 = new MusicCube();
        Cube71_5.color = 1;
        Cube71_5.posSpawn = 3;
        Cube71_5.timeSpawn = 0;
        Cube71_5.rotation = 3;
        cubeGenerator.musicCubes.Add(Cube71_5);

        //

        MusicCube musicCube99 = new MusicCube();
        musicCube99.color = 0;
        musicCube99.posSpawn = 3;
        musicCube99.timeSpawn = 2.4f;
        musicCube99.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube99);

        MusicCube musicCube100 = new MusicCube();
        musicCube100.color = 1;
        musicCube100.posSpawn = 2;
        musicCube100.timeSpawn = 0.17f;
        musicCube100.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube100);

        MusicCube musicCube101 = new MusicCube();
        musicCube101.color = 1;
        musicCube101.posSpawn = 0;
        musicCube101.timeSpawn = 0.25f;
        musicCube101.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube101);

        MusicCube musicCube102 = new MusicCube();
        musicCube102.color = 1;
        musicCube102.posSpawn = 2;
        musicCube102.timeSpawn = 2;
        musicCube102.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube102);

        MusicCube musicCube103 = new MusicCube();
        musicCube103.color = 0;
        musicCube103.posSpawn = 3;
        musicCube103.timeSpawn = 0.17f;
        musicCube103.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube103);

        MusicCube musicCube104 = new MusicCube();
        musicCube104.color = 0;
        musicCube104.posSpawn = 1;
        musicCube104.timeSpawn = 0.25f;
        musicCube104.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube104);

        MusicCube musicCube105 = new MusicCube();
        musicCube105.color = 0;
        musicCube105.posSpawn = 3;
        musicCube105.timeSpawn = 2;
        musicCube105.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube105);

        MusicCube musicCube106 = new MusicCube();
        musicCube106.color = 1;
        musicCube106.posSpawn = 2;
        musicCube106.timeSpawn = 0.17f;
        musicCube106.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube106);

        MusicCube musicCube107 = new MusicCube();
        musicCube107.color = 1;
        musicCube107.posSpawn = 0;
        musicCube107.timeSpawn = 0.25f;
        musicCube107.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube107);

        MusicCube musicCube108 = new MusicCube();
        musicCube108.color = 1;
        musicCube108.posSpawn = 2;
        musicCube108.timeSpawn = 2.1f;
        musicCube108.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube108);

        MusicCube musicCube109 = new MusicCube();
        musicCube109.color = 0;
        musicCube109.posSpawn = 3;
        musicCube109.timeSpawn = 0.17f;
        musicCube109.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube109);

        MusicCube musicCube110 = new MusicCube();
        musicCube110.color = 0;
        musicCube110.posSpawn = 1;
        musicCube110.timeSpawn = 0.25f;
        musicCube110.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube110);

        //Como yo soy

        MusicCube musicCube111 = new MusicCube();
        musicCube111.color = 0;
        musicCube111.posSpawn = 3;
        musicCube111.timeSpawn = 1.96f;
        musicCube111.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube111);

        MusicCube musicCube112 = new MusicCube();
        musicCube112.color = 0;
        musicCube112.posSpawn = 2;
        musicCube112.timeSpawn = 0.29f;
        musicCube112.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube112);

        MusicCube musicCube113 = new MusicCube();
        musicCube113.color = 1;
        musicCube113.posSpawn = 0;
        musicCube113.timeSpawn = 0.3f;
        musicCube113.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube113);

        MusicCube musicCube114 = new MusicCube();
        musicCube114.color = 1;
        musicCube114.posSpawn = 1;
        musicCube114.timeSpawn = 0.3f;
        musicCube114.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube114);

        MusicCube musicCube115 = new MusicCube();
        musicCube115.color = 0;
        musicCube115.posSpawn = 3;
        musicCube115.timeSpawn = 1.7f;
        musicCube115.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube115);

        MusicCube musicCube116 = new MusicCube();
        musicCube116.color = 1;
        musicCube116.posSpawn = 2;
        musicCube116.timeSpawn = 0.2f;
        musicCube116.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube116);

        MusicCube musicCube117 = new MusicCube();
        musicCube117.color = 1;
        musicCube117.posSpawn = 0;
        musicCube117.timeSpawn = 0.2f;
        musicCube117.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube117);

        MusicCube musicCube118 = new MusicCube();
        musicCube118.color = 0;
        musicCube118.posSpawn = 1;
        musicCube118.timeSpawn = 1.2f;
        musicCube118.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube118);

        MusicCube musicCube119 = new MusicCube();
        musicCube119.color = 1;
        musicCube119.posSpawn = 0;
        musicCube119.timeSpawn = 1.2f;
        musicCube119.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube119);

        MusicCube musicCube120 = new MusicCube();
        musicCube120.color = 0;
        musicCube120.posSpawn = 1;
        musicCube120.timeSpawn = 1.2f;
        musicCube120.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube120);

        MusicCube musicCube121 = new MusicCube();
        musicCube121.color = 1;
        musicCube121.posSpawn = 0;
        musicCube121.timeSpawn = 1.2f;
        musicCube121.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube121);

        MusicCube musicCube122 = new MusicCube();
        musicCube122.color = 0;
        musicCube122.posSpawn = 3;
        musicCube122.timeSpawn = 2.1f;
        musicCube122.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube122);

        MusicCube musicCube123 = new MusicCube();
        musicCube123.color = 1;
        musicCube123.posSpawn = 2;
        musicCube123.timeSpawn = 0.17f;
        musicCube123.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube123);

        MusicCube musicCube123_5 = new MusicCube();
        musicCube123_5.color = 1;
        musicCube123_5.posSpawn = 0;
        musicCube123_5.timeSpawn = 0.25f;
        musicCube123_5.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube123_5);

        MusicCube musicCube124 = new MusicCube();
        musicCube124.color = 1;
        musicCube124.posSpawn = 2;
        musicCube124.timeSpawn = 2.1f;
        musicCube124.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube124);

        MusicCube musicCube125 = new MusicCube();
        musicCube125.color = 0;
        musicCube125.posSpawn = 3;
        musicCube125.timeSpawn = 0.17f;
        musicCube125.rotation = 3;
        cubeGenerator.musicCubes.Add(musicCube125);

        MusicCube musicCube126 = new MusicCube();
        musicCube126.color = 0;
        musicCube126.posSpawn = 1;
        musicCube126.timeSpawn = 0.25f;
        musicCube126.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube126);

        MusicCube musicCube127 = new MusicCube();
        musicCube127.color = 0;
        musicCube127.posSpawn = 3;
        musicCube127.timeSpawn = 2.1f;
        musicCube127.rotation = 2;
        cubeGenerator.musicCubes.Add(musicCube127);

        MusicCube musicCube128 = new MusicCube();
        musicCube128.color = 1;
        musicCube128.posSpawn = 2;
        musicCube128.timeSpawn = 0.17f;
        musicCube128.rotation = 1;
        cubeGenerator.musicCubes.Add(musicCube128);

        MusicCube musicCube129 = new MusicCube();
        musicCube129.color = 1;
        musicCube129.posSpawn = 0;
        musicCube129.timeSpawn = 0.25f;
        musicCube129.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube129);

        MusicCube musicCube130 = new MusicCube();
        musicCube130.color = 0;
        musicCube130.posSpawn = 1;
        musicCube130.timeSpawn = 1.2f;
        musicCube130.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube130);

        //Palla'Voooooooooy

        MusicCube musicCube131 = new MusicCube();
        musicCube131.color = 1;
        musicCube131.posSpawn = 0;
        musicCube131.timeSpawn = 1.2f;
        musicCube131.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube131);

        MusicCube musicCube132 = new MusicCube();
        musicCube132.color = 0;
        musicCube132.posSpawn = 1;
        musicCube132.timeSpawn = 2.7f;
        musicCube132.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube132);

        MusicCube musicCube133 = new MusicCube();
        musicCube133.color = 1;
        musicCube133.posSpawn = 0;
        musicCube133.timeSpawn = 2.4f;
        musicCube133.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube133);

        MusicCube musicCube134 = new MusicCube();
        musicCube134.color = 0;
        musicCube134.posSpawn = 1;
        musicCube134.timeSpawn = 2.2f;
        musicCube134.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube134);

        MusicCube musicCube135 = new MusicCube();
        musicCube135.color = 1;
        musicCube135.posSpawn = 0;
        musicCube135.timeSpawn = 0.2f;
        musicCube135.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube135);

        MusicCube musicCube136 = new MusicCube();
        musicCube136.color = 0;
        musicCube136.posSpawn = 1;
        musicCube136.timeSpawn = 2.3f;
        musicCube136.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube136);

        MusicCube musicCube137 = new MusicCube();
        musicCube137.color = 1;
        musicCube137.posSpawn = 0;
        musicCube137.timeSpawn = 0.2f;
        musicCube137.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube137);

        MusicCube musicCube138 = new MusicCube();
        musicCube138.color = 0;
        musicCube138.posSpawn = 1;
        musicCube138.timeSpawn = 2.3f;
        musicCube138.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube138);

        MusicCube musicCube139 = new MusicCube();
        musicCube139.color = 1;
        musicCube139.posSpawn = 0;
        musicCube139.timeSpawn = 0.2f;
        musicCube139.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube139);

        MusicCube musicCube140 = new MusicCube();
        musicCube140.color = 0;
        musicCube140.posSpawn = 1;
        musicCube140.timeSpawn = 2.3f;
        musicCube140.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube140);

        MusicCube musicCube141 = new MusicCube();
        musicCube141.color = 1;
        musicCube141.posSpawn = 0;
        musicCube141.timeSpawn = 0.2f;
        musicCube141.rotation = 0;
        cubeGenerator.musicCubes.Add(musicCube141);

        XMLOp.Serialize(cubeGenerator, getPath(cubeGenerator.musicName));
    }

    private string getPath(string musicName)
    {
        #if UNITY_EDITOR
            return Application.dataPath + "/CubeGenerators/" + musicName + ".xml";
        #elif UNITY_ANDROID
            return Application.persistentDataPath+musicName+".xml";
        #elif UNITY_IPHONE
            return Application.persistentDataPath+"/"+musicName".xml";
        #else
            return Application.dataPath + "/" + musicName +".xml";
        #endif
    }
}
