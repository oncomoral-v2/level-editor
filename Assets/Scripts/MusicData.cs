using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicData : MonoBehaviour
{
    public AudioClip audioClip;
    public float[] musicData;

    // Start is called before the first frame update
    void Start()
    {
        float[] samples = new float[audioClip.samples * audioClip.channels];
        musicData = new float[audioClip.samples * audioClip.channels];
        audioClip.GetData(samples, 0);
        musicData = samples;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
