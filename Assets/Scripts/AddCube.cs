using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

//G�re l'ajout des cubes dans l'�diteur de niveaux
//Appliqu� sur l'objet CubePlacer dans la sc�ne
//Appel� lorsqu'on appuie sur les diff�rents boutons du sous-menu CubeAdder
public class AddCube : MonoBehaviour
{
    public GameObject[] cubes;
    public Transform[] points;
    private float time;
    public GameObject cubeParent;
    public Slider mainSlider;
    public MusicSlider musicSlider;
    public GameManager gameManager;

    public Image cubeImage;

    public Image colorRectangle;

    public Image buttonImage;

    //public GameObject text;

    private bool isBlue = true;

    private int rot = 0;

    public void PlaceCube(int pos)
    {
        //Ajoute un cube de couleur col � la position pos, et la rotation rot
        int colIndex = 1;
        float localPosz = 12;
        float offSet = localPosz / GameManager.cubeSpeed;
        if (isBlue)
        {
            colIndex = 0;
        }
        if (Collide(pos))
        {
            //Joue une petite animation sur le slider si le cube qu'on souhaite ajouter est en collision avec un autre
            Debug.Log("Collision");
            LeanTween.cancel(mainSlider.gameObject);
            LeanTween.moveX(mainSlider.gameObject, mainSlider.transform.position.x - 3, 0.5f).setEaseShake();
        }

        else
        {
            GameObject cube = Instantiate(cubes[colIndex], points[pos]);
            cube.transform.localPosition = new Vector3(0, 0, -localPosz);
            cube.transform.Rotate(transform.forward, 90 * rot);
            cube.transform.parent = cubeParent.transform;
            cube.GetComponent<Movement>().mainSlider = mainSlider;
            cube.GetComponent<Movement>().musicSlider = musicSlider;
            MusicCube musicCube = new MusicCube();
            musicCube.type = 0;
            musicCube.color = colIndex;
            musicCube.posSpawn = pos;
            musicCube.rotation = rot;
            musicCube.timeSpawn = GameManager.timer - offSet;
            cube.GetComponent<Movement>().startPos = points[pos].position;
            cube.GetComponent<Movement>().startPos.z += GameManager.cubeSpeed * musicCube.timeSpawn;
            //cube.GetComponent<Movement>().timer = GameManager.timer;
            GameManager.currentCubes.Add(cube);
            //musicCube.index = GameManager.currentCubes.Count;
            GameManager.musicCubes.Add(musicCube);
            gameManager.Sort();
        }
        
    }

    public void ChangeColor()
    {
        //Change la couleur du cube � ajouter
        isBlue = !isBlue;
        if (isBlue)
        {
            colorRectangle.color = Color.blue;
            buttonImage.color = Color.red;
        }
        else
        {
            colorRectangle.color = Color.red;
            buttonImage.color = Color.blue;
        }
    }

    public void Rotate()
    {
        //Change la rotation du cube � ajouter
        if (rot < 3)
        {
            rot++;
        }
        else
        {
            rot = 0;
        }

        cubeImage.rectTransform.rotation = Quaternion.Euler(new Vector3(cubeImage.rectTransform.rotation.x, cubeImage.rectTransform.rotation.y, 90 * rot));
    }

    private bool Collide(int pos)
    {
        //Retourne true si le cube que l'on souhaite ajouter est en collision avec un autre
        float localPosz = 12;
        float offSet = localPosz / GameManager.cubeSpeed;
        float timeSpawn = GameManager.timer - offSet;
        //localPosz += GameManager.cubeSpeed * timeSpawn;

        float timeWindow = 1 / GameManager.cubeSpeed;

        foreach (MusicCube musicCube in GameManager.musicCubes)
        {
            if (pos == musicCube.posSpawn)
            {
                if (System.Math.Abs(timeSpawn - musicCube.timeSpawn) < timeWindow)
                {
                    return true;
                }
            }           
        }

        return false;
    }
    /*public void Copy()
    {
        //Permet de "copier" les cubes s�lectionn�s
        List<int> copiedIds = new List<int>();
        foreach (GameObject cube in GameManager.selectedCubes)
        {
            if (!copiedIds.Contains(cube.GetComponent<Movement>().id))
            {
                copiedIds.Add(cube.GetComponent<Movement>().id);
            }
                
        }

        foreach (MusicCube musicCube in GameManager.musicCubes)
        {
            if (copiedIds.Contains(musicCube.index))
            {
                GameManager.copiedCubes.Add(musicCube);
            }
        }

        //Debug.Log(GameManager.copiedCubes.Count);
    }

    public void Paste()
    {
        //"Colle" les cubes copi�s � l'instant o� on a fait pause
        //Debug.Log(GameManager.copiedCubes.Count);
        float newTime = GameManager.timer;
        float minTime = GameManager.copiedCubes.Min(c => c.timeSpawn);
        float offSetTime = newTime - minTime;
        float localPosz = 12;
        float offSet = localPosz / GameManager.cubeSpeed;
        foreach (MusicCube musicCube in GameManager.copiedCubes)
        {
            GameObject cube = Instantiate(cubes[musicCube.color], points[musicCube.posSpawn]);
            cube.transform.Rotate(transform.forward, 90 * musicCube.rotation);
            cube.transform.localPosition = new Vector3(0, 0, -localPosz + (musicCube.timeSpawn - minTime) * GameManager.cubeSpeed);
            cube.transform.parent = cubeParent.transform;
            cube.GetComponent<Movement>().mainSlider = mainSlider;
            cube.GetComponent<Movement>().musicSlider = musicSlider;
            MusicCube pastedCube = musicCube;
            pastedCube.timeSpawn += offSetTime - offSet;
            //pastedCube.timeSpawn = GameManager.timer - offSetTime;
            cube.GetComponent<Movement>().startPos = points[musicCube.posSpawn].position;
            cube.GetComponent<Movement>().startPos.z += GameManager.cubeSpeed * pastedCube.timeSpawn;
            GameManager.currentCubes.Add(cube);
            GameManager.musicCubes.Add(pastedCube);            
        }

        gameManager.Sort();
    }*/
}
