using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Permet de quitter l'application
//Appel� par le bouton Quit
public class Quit : MonoBehaviour
{
    public void QuitApplication()
    {
        Application.Quit();
    }
}
